from random import random
from fastapi import FastAPI
import requests
import time

app = FastAPI()
count = 0

@app.get("/{page}")
async def root(page):
    if not page:
        page = 1
    response = requests.get('https://rickandmortyapi.com/api/character/?page='+ page)
    return response.json()


@app.get("/unstable/{page}")
async def root(page):

    if round(random() * 5) == 0:
        raise Exception('failed')

    if round(random() * 5) == 1:
        print('hang')
        time.sleep(100)
    
    if not page:
        page = 1
    response = requests.get('https://rickandmortyapi.com/api/character/?page='+ page)
    return response.json()