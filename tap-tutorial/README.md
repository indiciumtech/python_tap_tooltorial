# O que é um tap

Pela documentação:

'
A Tap is an application that takes a configuration file and an optional state file as input and produces an ordered stream of record, state and schema messages as output. A record is json-encoded data of any kind. A state message is used to persist information between invocations of a Tap. A schema message describes the datatypes of the records in the stream. A Tap may be implemented in any programming language.

Taps are designed to produce a stream of data from sources like databases and web service APIs for use in a data integration or ETL pipeline
'

https://github.com/singer-io/getting-started/blob/master/docs/SPEC.md


Um tap então é basicamente um stream(fluxo) de dados com 3 tipos de mensagens:

- schema
- record
- state

# Escrevendo Nosso tap

## Preparando o ambiente

Recomendamos o uso de:
  - vscode como editor de texto/ide
  - venv para gerenciar o ambiente python

Crie e ative um virtual env:

```
python3 -m venv venv
source venv/bin/activate
```

instale as dependencias que vamos utilizar no código que por enquanto são somente a lib para desenvovimento de taps singer-python e 
a lib fastapi para a api demo

```
python setup.py install
```

## Os Dados

Para desenvolvimento desse tutorial vamos utilizar um conjunto de dados de personagens de rick e morty.
Para servir esses dados existe uma api na pasta demo_api deste repositório, rode essa api abrindo uma aba do seu terminal
e executando 

```
cd demo_api
uvicorn main:app --reload
```

deixe essa aba rodando, para testar a api, abra outra aba do terminal e execute

```
curl localhost:8000/1
```

o resultado é um json com algumas informações sobre personagens de rick e morty. Para visualizar melhor podemos redirecionar o resultado da api para um arquivo e
abrir esse arquivo no vscode, no outro terminal


```
curl localhost:8000/1 > data.json
```

Ao abrir esse arquivo no code, ctrl + shift + p e digitar format document. Ajuda bastante a visualização


## O schema

Conforme vimos na specifição, a primeira mensagem de um tap deve ser o schema, e o protocolo do singer usa o jsonschema para isso.

A especificação do jsonschema pode ser encontrada em https://json-schema.org/ , e vale a pena dar uma lida em https://json-schema.org/learn/getting-started-step-by-step.html pra entender a ideia geral

Dos dados da nossa api em questão, o dado em si tem o seguinte o formato:

```
 {
    "id": 7,
    "name": "Abradolf Lincler",
    "status": "unknown",
    "species": "Human",
    "type": "Genetic experiment",
    "gender": "Male",
    "origin": {
        "name": "Earth (Replacement Dimension)",
        "url": "https://rickandmortyapi.com/api/location/20"
    },
    "location": {
        "name": "Testicle Monster Dimension",
        "url": "https://rickandmortyapi.com/api/location/21"
    },
    "image": "https://rickandmortyapi.com/api/character/avatar/7.jpeg",
    "episode": [
        "https://rickandmortyapi.com/api/episode/10",
        "https://rickandmortyapi.com/api/episode/11"
    ],
    "url": "https://rickandmortyapi.com/api/character/7",
    "created": "2017-11-04T19:59:20.523Z"
}
```

gerar um jsonschema na mão pode ser tedioso e existem algumas ferramentas online para isso, pra esse tutorial podemos usar:
https://json-schema-inferrer.herokuapp.com/~~
~~
o json gerado colocaremos em tap_tutorial/schemas/

O singer python tem uma funcionalidade discover, que gera um catalogo configurado. Um catalogo pode ter um ou mais stream de dados.
Vamos gerar o nosso catalogo.
No outro terminal, entre no venv, caso ainda não esteja, e entre na pasta tap-tutorial

```
source venv/bin/activate
cd tap-tutorial
python -m tap_tutorial.__init__ -c demo_config.json --discover > catalog.json
```

Com o schema em mãos, podemos de fato gerar os dados

## Executando o sync

agora que temos o config e catalog em mãos podemos executar de fato o tap:

```
python -m tap_tutorial.__init__ -c demo_config.json
```

A mensagem que retorna é

```
INFO Skipping stream: characters
```

Isso significa que o stream nao está selecionado, para isso adicionamos o campo 
selected: true 
dentro da propriedade schema do do stream, da seguinte forma. Em catalog.json:
``
      {
 "streams": [
    {
      "tap_stream_id": "characters",
     "key_properties": [],
     "schema": {
       "selected": true,
       "properties": {
         "image": {
           "type": "string"

``

Agora tentamos denovo:

```
python -m tap_tutorial.__init__ -c demo_config.json --catalog catalog.json
```

E agora temos uma mensagem terrível

## Configurando debugger do code

Facilita muito no desenvolvimento configurar o debugger do code para debugar o código.
vá no incone do inseto na barra da esquerda do code(Run and Debug) e clique em 'create a launch.json file'
no dropdown escolha python, e depois escolha python module.

Nós ja rodamos o nosso código como um módulo do python:
```
python -m tap_tutorial.__init__ -c demo_config.json
```
Então ali colocamamos a mesma coisa:
tap_tutorial.__init__

para colocar o config.json e o catalog.json como argumentos usamos o o campo args, um array de string sendo cada string um argumento

# tap-tutorial

Corrigindo os Bugs:
__init__.py , linha 74
``
        for row in fetch_characters_data():
``
api_client.py , linha 5

``
    return requests.get("http://localhost:8000/1").json()
``

This is a [Singer](https://singer.io) tap that produces JSON-formatted data
following the [Singer
spec](https://github.com/singer-io/getting-started/blob/master/SPEC.md).


## Tópicos Intermediarios em Consumo de APIs

### Paginação

Boa parte das apis retornam as respostas em páginas, e para pegar todas as mensagens precisamos implementar uma estratégia adequada.
Nem todas as apis retornam a informação sobre as páginas da mesma forma.
A api demo desse tutorial retorna um objeto info com as informações de paginação:

```
"info": {
    "count": 826,
    "pages": 42,
    "next": "https://rickandmortyapi.com/api/character/?page=2",
    "prev": null
  }
```

sabemos que o endpoint tem 42 páginas, se buscarmos a página 42 no campo next temos null.

```
curl localhost:8000/42

## resposta:
{
  "info": {
    "count": 826,
    "pages": 42,
    "next": null,
    "prev": "https://rickandmortyapi.com/api/character/?page=41"
}
```

vamos escrever o código para pegar todos as páginas.

[colocar resposta aqui]

### Mensagens Grandes Demais

Para simular um ambiente com memória limitada, vamos rodar um container docker da seguinte forma:

docker run -it --entrypoint bash --entrypoint  bash --network host -w /opt/tap-tutorial/tap_tutorial --memory 30m -v $PWD:/opt/ python-requests
[explicar melhor aqui esse comando e passar pra composer]
[tem que rodar da pasta python-tap-tooltorial, root do projeto]

### Abordagens para Apis Instaveis

Retries e exponential backoff





