#!/usr/bin/env python
from setuptools import setup

setup(
    name="tap-tutorial",
    version="0.1.0",
    description="Singer.io tap for extracting data",
    author="Stitch",
    url="http://singer.io",
    classifiers=["Programming Language :: Python :: 3 :: Only"],
    py_modules=["tap_tutorial"],
    install_requires=[
        # NB: Pin these to a more specific version for tap reliability
        "singer-python",
        "requests",
        # Apenas para levantar a api, nao relacionado ao tap
        "fastapi[all]"
    ],
    entry_points="""
    [console_scripts]
    tap-tutorial=tap_tutorial:main
    """,
    packages=["tap_tutorial"],
    package_data = {
        "schemas": ["tap_tutorial/schemas/*.json"]
    },
    include_package_data=True,
)
