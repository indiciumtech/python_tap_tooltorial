from doctest import REPORT_NDIFF
from urllib import request 

import logging
logging.basicConfig()
logging.getLogger().setLevel(logging.DEBUG)
requests_log = logging.getLogger("requests.packages.urllib3")
requests_log.setLevel(logging.DEBUG)
requests_log.propagate = True

import requests
import json

from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry

DEFAULT_CONNECTION_TIMEOUT = 1

def requests_retry_session(
    retries=2,
    backoff_factor=100,
    status_forcelist=(500, 502, 503, 504),
    session=None,
):
    session = session or requests.Session()
    retry = Retry(
        total=retries,
        read=retries,
        connect=retries,
        backoff_factor=backoff_factor,
        status_forcelist=status_forcelist,
    )

    adapter = HTTPAdapter(max_retries=retry)
    session.mount('http://', adapter)
    session.mount('https://', adapter)
    return session


def fetch_characters_data(page=1):
    api_url = 'http://localhost:9000/unstable'
    session = requests_retry_session()
    
    response = session.get(f"{api_url}/{page}", timeout=2).json()
    num_pages = response['info']['pages']

    characters = []
    for page in range(2, num_pages + 1):
        response_json = session.get(f"{api_url}/{page}", timeout=2).json()
      
        characters.append(str(response_json['results'] * 100 ))
        
    return characters


if __name__ == "__main__":
    [print(json.dumps(character)) for character in fetch_characters_data()]