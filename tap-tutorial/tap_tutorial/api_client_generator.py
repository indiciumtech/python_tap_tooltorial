import requests
import json
import time

# TODO: delete and replace this inline function with your own data retrieval process:
def fetch_characters_data(page=1):
    api_url = 'http://localhost:8000'
    
    # Returning first page
    response = requests.get(f"{api_url}/{page}").json()
    yield response['results']

    num_pages = response['info']['pages']

    for page in range(2, num_pages + 1):
        print(f"getting page {page}")
        response = requests.get(f"{api_url}/{page}").json()
        yield str(response['results'] * 100 )
        
        # if page == 15:
        #     break
    
    
if __name__ == "__main__":
    [print(json.dumps(character)) for character in fetch_characters_data()]