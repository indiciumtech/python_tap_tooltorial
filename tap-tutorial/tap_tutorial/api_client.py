import requests

def fetch_characters_data():
    response = requests.get("http://localhost:9000/unstable/1").json()
    
    if response.get('info') is None:
        raise Exception("response does not contain pagination")
    
    number_of_pages = response.get('info').get('pages')

    # First page itens
    response_itens = response['results']
    
    for page in range(2, number_of_pages + 1):
        response = requests.get("http://localhost:9000/unstable/" + str(page)).json()
        response_itens = response_itens + response['results']

    return response_itens    

if __name__ == "__main__":
    print("Running")
    result = fetch_characters_data()
    print(len(result))
    # print(result)