# Python Tap Tooltorial

Neste tooltorial vamos aprender conceitos de python, APIs e integração de dados através de um projeto real.


## Instalando o python

Para realizar esse projeto você deve ter o Python instalado e disponível no *path* do seu sistema operacional. Para instalar o Python siga as instruções para cada sistema operacional:

[Linux](https://python.org.br/instalacao-linux/)

[Windows](https://python.org.br/instalacao-windows/)

[Mac](https://python.org.br/instalacao-mac/)

## Configurando o ambiente

A primeira tarefa ao trabalhar em um projeto Python é configurar um ambiente virtual. Um ambiente virtual é uma ferramenta que isola as distribuições de pacotes usados em um projeto dos demais pacotes instalados em sua máquina. Você pode ainda não entender a importância disso, mas seguramente vai lhe poupar de muitas dores de cabeça no futuro.

Para criar um ambiente virtual basta rodar o comando abaixo na pasta do projeto:

```
python3 -m venv venv
```

Você pode notar que uma nova pasta chamada `venv` foi criada. Será dentro dessa pasta que os pacotes do nosso projeto serão instalado. Para ativá-la basta rodar o comando:

```
source venv/bin/activate
```

Pronto! Agora você está trabalhando dentro de um ambiente virtual python e pode identificá-lo no seu terminal pelo `(venv)` antes do caminho do terminal:
 
```
(venv) danielavancini@pop-os:~/Projects/tooltorials/python_tap_tooltorial$
```

## Iniciando nosso projeto

Na 1a parte do nosso projeto vamos aprender como se comunicar com um REST API usando o python. APIs são servidores que permitem enviar e requisitar dados usando código e são basicamente o idioma com que os diferentes sistemas se conversam na web.

Antes de começar a escrever nosso código, precisamos criar nosso script python. Fazemos isso através do comando abaixo (ou pela interface da sua IDE):

```
touch my_first_script.py
```

Para consultar nosso API vamos instalar um pacote python que possui funções específicas para isso chamado `requests`. Vamos utilizar a ferramenta `pip` que nos permite instalar bibliotecas diretamente do terminal.
Apesar de ser possivel instalar com apenas uma linha usando pip install, é muito importante deixar registrado as dependencias de um projeto, então para reforçar essa pratica vamos criar um requirements.txt
e especificar nossa lib e sua versao.
Crie um arquivo requirements.txt e adicione a linha

```
requests==2.25.1
```
e na sequencia execute

```
pip install -r requirements.txt

```

Agora podemos importar essa biblioteca em nosso script para utilizá-la em nosso projeto:

```python 
import requests
```

## Chamando nossa primeira API

Vamos começar nosso projeto com um API muito simples que trás informações sobre a ISS,a estação espacial internacional. Essa API possui um endpoint (uma espécie de endereço de uma API) sem autenticação e que nos retorna a localização da ISS neste momento:

```python

# Create Response object
response = requests.get("http://api.open-notify.org/iss-now.json")

# Print response text to stdout
print(response.text)
```

Ao executarmos nosso script verificamos que a API nos retornou um texto em formato JSON com a latitude e longitude da estação neste exato momento:

```python
{"message": "success", "iss_position": {"latitude": "-16.6551", "longitude": "168.0583"}, "timestamp": 1644263408}
```

Mas se ao invés de saber a localização da ISS nosso objetivo for saber quantas pessoas estão no espaço neste momento? Para isso vamos usar outra API (também sem autenticação):

```python

# Create Response object
response = requests.get("http://api.open-notify.org/astros.json")

# Print response text to stdout
print(response.text)
```

## Trabalhando com objetos JSON em python

Em nosso primeiro exemplo nós mencionamos que a resposta da API era um texto em formato JSON. O JSON (JavaScript Object Notation) é a linguagem das APIs, assim como um CSV é provavelmente a linguagem das tabelas. Ele é uma forma de codificar os dados de forma a facilitar o entendimento por outras máquinas (ainda que não ajude muito a nós humanos). Para trabalhar com esse tipo de estrutura de dado o Python possui uma biblioteca muito poderosa chamada (pasmem) de `json`. Vamos usar duas funções dessa biblioteca:

- json.dumps() - Pega um objeto Python e converte em um texto em formato JSON. 
- json.load() -  Pega um texto em formato JSON e converte em um objeto Python

No exemplo abaixo, vemos como a função `dumps` é útil para retornar o objeto em um formato mais agradável para visualizarmos:

```python
import requests
import json

# Create Response object
response = requests.get("http://api.open-notify.org/astros.json")

# Converts response to json
response_json = response.json()

# Converts json to a formatted string
text= json.dumps(response_json, sort_keys=True, indent=4)

# Print
print(text)
```
```
{
    "message": "success",
    "number": 10,
    "people": [
        {
            "craft": "ISS",
            "name": "Mark Vande Hei"
        },
        {
            "craft": "ISS",
            "name": "Pyotr Dubrov"
        },
        {
            "craft": "ISS",
            "name": "Anton Shkaplerov"
        },
        {
            "craft": "Shenzhou 13",
            "name": "Zhai Zhigang"
        },
        {
            "craft": "Shenzhou 13",
            "name": "Wang Yaping"
        },
        {
            "craft": "Shenzhou 13",
            "name": "Ye Guangfu"
        },
        {
            "craft": "ISS",
            "name": "Raja Chari"
        },
        {
            "craft": "ISS",
            "name": "Tom Marshburn"
        },
        {
            "craft": "ISS",
            "name": "Kayla Barron"
        },
        {
            "craft": "ISS",
            "name": "Matthias Maurer"
        }
    ]
}
```

Em geral, queremos exportar o resultado como um arquivo e não apenas imprimir o resultado no terminal. Para isso vamos adicionar mais um bloco de código que vai exportar o resultado em um arquivo `output.json`

```python
# Writing to sample.json
with open("output.json", "w") as outfile:
    outfile.write(text)
```

Agora vamos usar as ferramentas do Python para processar nosso objeto `json` usando estruturas de dados familiares: listas e dicionários. Se olharmos o resultado da consulta dos astronautas da ISS, conseguimos identificar uma lista de dicionários que contém o nome da espaçonave e do astronauta:

```python
   "people": [
        {
            "craft": "ISS",
            "name": "Mark Vande Hei"
        },
        (...)
   ]
```
Para converter esse texto em um objeto Python utilizaremos a função `loads`:

```python
# Converts response text to python object
response_dict= json.loads(response.text)
```

Vamos exportar somente os nomes dos Astronautas:

```python
#Iterate over list and print names

for i in response_dict['people']: 
    print(i['name'])
```

Agora vamos salvar as espaçonaves em uma nova lista:

```python
#Iterate over list and save to a new list of crafts
crafts = []
for i in response_dict['people']:
    crafts.append(i['craft'])

print(crafts)
```

## Exportando para CSV


```python
import csv

# Save to CSV
fields = ['crafts','names']
rows = []

for i in response_dict['people']:
    rows.append([i['craft'], i['name']])

with open("output.csv",'w') as f:
    write = csv.writer(f)
    write.writerow(fields)
    write.writerows(rows)
```

# Tutorial de Singer Tap

Agora que você já sabe como extrair dados de uma fonte na Web usando Python, vamos introduzir o conceito de Singer Taps, uma especificação que facilita a integração de dados. 

Para acessar o tutorial use este [link](tap-tutorial/README.md).

# Referências

Esse tooltorial foi baseado em alguns exemplos disponíveis na web:

[Dataquest](https://www.dataquest.io/blog/python-api-tutorial/)